import React from 'react';

// Media assets
import logoLight from "../../assets/images/logo-light.svg";

export default function Header() {
  // Render
  return (
    <header className={"header"}>
      <a href="#">
        <img src={logoLight} alt="Logo" />
      </a>

      <div className="search-bar">
        <input placeholder="Search" />
        <a to="/search-page">Search</a>
      </div>
    </header>
  );
}