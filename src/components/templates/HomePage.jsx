import React from 'react';
//import my components
import Card from '../molecules/Card';

import Header from '../organisms/Header';


export default function HomePage({ information}) {

    const Cards = information.map((item) => {
        return <Card key={item.id} data={item} />;
      });
      

return (
    <div className= "home-page">
        <Header />
        <section className="recomended">
            <h2>Recommended Videos</h2>
            <div className="grid"> {Cards} </div>
        </section>
        
    </div>

);

}