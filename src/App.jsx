import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
//Components
import HomePage from "./components/templates/HomePage";
import VideoPage from "./components/templates/videoPage";

import "./css/style.css";

import information from "./information.json";


function App() {

  

  return (
    <Router>
      <div className="App">
        <Switch>
          <Route
            path="/"
            exact
            render={() => <HomePage information={information} />}
          />
          <Route
            path="/video/:id"
            render={({ match }) => (
              <VideoPage match={match} information={information} />
            )}
          />
        </Switch>
      </div>
    </Router>
  );
}

export default App;
